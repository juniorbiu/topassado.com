-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 23-Jan-2019 às 07:14
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tp_bd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tp_imagens`
--

CREATE TABLE `tp_imagens` (
  `id` int(11) NOT NULL,
  `id_questao` int(11) NOT NULL,
  `nome` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tp_provas`
--

CREATE TABLE `tp_provas` (
  `id` int(11) NOT NULL,
  `descricao` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ano` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tp_provas`
--

INSERT INTO `tp_provas` (`id`, `descricao`, `ano`) VALUES
(25, 'Exame Seletivo de 2010', 2010);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tp_questoes`
--

CREATE TABLE `tp_questoes` (
  `id` int(11) NOT NULL,
  `enunciado` varchar(500) CHARACTER SET utf8 NOT NULL,
  `disciplina` varchar(100) DEFAULT NULL,
  `alt_a` varchar(200) CHARACTER SET utf8 NOT NULL,
  `alt_b` varchar(200) CHARACTER SET utf8 NOT NULL,
  `alt_c` varchar(200) CHARACTER SET utf8 NOT NULL,
  `alt_d` varchar(200) CHARACTER SET utf8 NOT NULL,
  `alt_correta` varchar(1) CHARACTER SET utf8 NOT NULL,
  `id_prova` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tp_resultados`
--

CREATE TABLE `tp_resultados` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `descricao` int(50) NOT NULL,
  `qtd_questoes` int(11) NOT NULL,
  `corretas_pt` int(11) NOT NULL,
  `corretas_mat` int(11) NOT NULL,
  `corretas_total` int(11) NOT NULL,
  `data` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tp_resultados`
--

INSERT INTO `tp_resultados` (`id`, `id_usuario`, `descricao`, `qtd_questoes`, `corretas_pt`, `corretas_mat`, `corretas_total`, `data`) VALUES
(1, 4, 0, 1, 0, 0, 0, '23/01/19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tp_usuarios`
--

CREATE TABLE `tp_usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sobrenome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(32) CHARACTER SET utf8 NOT NULL,
  `grupo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tp_usuarios`
--

INSERT INTO `tp_usuarios` (`id`, `nome`, `sobrenome`, `email`, `senha`, `grupo`) VALUES
(4, 'tanto faz', 'usuario', 'admin@admin.com', '92f20dafc5e5ac1c66820903c492cc04', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tp_imagens`
--
ALTER TABLE `tp_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_questao` (`id_questao`);

--
-- Indexes for table `tp_provas`
--
ALTER TABLE `tp_provas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tp_questoes`
--
ALTER TABLE `tp_questoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_prova` (`id_prova`);

--
-- Indexes for table `tp_resultados`
--
ALTER TABLE `tp_resultados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tp_usuarios`
--
ALTER TABLE `tp_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tp_imagens`
--
ALTER TABLE `tp_imagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tp_provas`
--
ALTER TABLE `tp_provas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tp_questoes`
--
ALTER TABLE `tp_questoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tp_resultados`
--
ALTER TABLE `tp_resultados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tp_usuarios`
--
ALTER TABLE `tp_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tp_imagens`
--
ALTER TABLE `tp_imagens`
  ADD CONSTRAINT `tp_imagens_ibfk_1` FOREIGN KEY (`id_questao`) REFERENCES `tp_questoes` (`id`);

--
-- Limitadores para a tabela `tp_questoes`
--
ALTER TABLE `tp_questoes`
  ADD CONSTRAINT `tp_questoes_ibfk_1` FOREIGN KEY (`id_prova`) REFERENCES `tp_provas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

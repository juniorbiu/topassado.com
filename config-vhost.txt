1 - V� na pasta xampp (seja l� onde ela estiver) e siga o passo a passo:
Caminho: xampp -> apache -> conf -> extra -> httpd.vhosts
Edite o arquivo, colando ao final essas configura��es:


<VirtualHost *:80>
    ServerAdmin topassado.com
    DocumentRoot "D:/xampp/htdocs/topassado.com"
    ServerName topassado.com
    ErrorLog "logs/topassado.com-error.log"
    CustomLog "logs/topassado.com-access.log" common
</VirtualHost>

Salve o arquivo.

--- Observa��o: Para funcionar, a pasta do projeto topassado.com tem que estar dentro do diret�rio htdocs.


2 - Edite o arquivo hosts, localizado em C:\Windows\System32\drivers\etc
Para editar, precisa executar o editor de texto (bloco de notas ou outro) como administrador.
cole no final do arquivo:
127.0.0.1	topassado.com

3 - Inicie ou reinicie o xampp, teste e seja feliz.
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simulado extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Questao_model');
	}

	public function index()
	{
		$this->load->view('usuario/resolversimulado_view');
    }
    
    public function randomQuestoes()
	{
		$qtdQuestoes = $this->input->post('qtdQuestoes');
		$dados['portugues'] = $this->Questao_model->ptQuestoes($qtdQuestoes);
		$dados['matematica'] = $this->Questao_model->matQuestoes($qtdQuestoes);
		echo json_encode($dados);
	}

}

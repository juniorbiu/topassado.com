<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuario_model');
	}

	public function Principal()
	{
		$this->load->view('usuario/princip_view');
	}

	public function Login()
	{
		$this->load->view('usuario/login_view');
	}

	public function Cadastro()
	{
		$this->load->view('usuario/cadastro_view');
	}


	public function autenticar()
	{
		// Recebendo dados do formulário

		$email = $this->input->post('email');
		$senha = md5($this->input->post('senha'));

		// Validação de formulário
		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
		$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[6]', 
		array('min_length' => 'A senha deve ter, no mínimo, 6 caracteres.'));

		// form_validation->run Retorna true se não houver erro de validação 
		// Se tiver erro de validação...
		if($this->form_validation->run() == false){
			$erros = array('mensagens' => validation_errors());
			$this->load->view('usuario/login_view', $erros);
		} else {
			// Sem nenhum erro de validação

			// Verifica se existe o usuário
			$status = $this->Usuario_model->autenticar($email, $senha);
			$usuario = array(
				'nome' => $status->nome,
				'sobrenome' => $status->sobrenome,
				'grupo' => $status->grupo,
				'id' => $status->id
			);
			$this->session->userdata('id', $status->id);
			if($status){
				// Cria session logado
				$this->session->set_userdata('usuario', $usuario);
				// Redireciona para a página de provas
				redirect(base_url('provas'));
			}else{
				$this->session->set_flashdata('erro', 'E-mail ou senha incorretos.');
				redirect(base_url('login'));
			}	
		}


		// $this->load->view('usuario/login_view', $dados);
	}


	public function cadastrar()
	{
		// Pega dados do formulário e armazena num array
		$dados = array(
			'nome' => $this->input->post('nome'),
			'sobrenome' => $this->input->post('sobrenome'),
			'email' => $this->input->post('email'),
			'senha' => md5($this->input->post('senha'))
		);

		// Validação de formulário

		// Regras para cada campos, com respectivos erros
		$this->form_validation->set_rules('nome', 'Nome', 'required');
		$this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|is_unique[tp_usuarios.email]', 
		array('valid_email' => 'Email inválido! (exemplo: email@servidor.com)',
		'is_unique' => 'Endereço de e-mail já cadastrado!'));
		$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[6]', 
		array('min_length' => 'A senha deve ter, no mínimo, 6 caracteres.'));
		$this->form_validation->set_rules('confSenha', 'Confirmação de senha', 'required|matches[senha]', 
		array('matches' => 'As senhas não são iguais.'));

		// form_validation->run Retorna true se não houver erro de validação 
		// Se tiver erro de validação...
		if($this->form_validation->run() == false){
			$erros = array('mensagens' => validation_errors());
			$this->load->view('usuario/cadastro_view', $erros);
		} else {
			// Sem nenhum erro de validação

			// Insere usuário na tabela
			$status = $this->Usuario_model->inserir($dados);

			// Cria mensagem de sucesso temporária
			$this->session->set_flashdata('cadastro', 'Usuário cadastrado com sucesso!');

			// Redireciona para a página de login
			redirect(base_url('login'));
		}

		

	}

}
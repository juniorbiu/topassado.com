<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Questao extends CI_Controller {

	// Método construtor
	function __construct(){
		parent::__construct();
		$this->load->model('Questao_model');
	}

	public function Index()
	{
		/*
		# Array $dados, índice 'questoes' recebe o retorno de getAll()
		*/
		$dados['questoes'] = $this->Questao_model->getAll();
		$this->load->view('admin/questoes_view', $dados);
	}

	/*
	* Método para exibir addquestao (formulário)
	*/
	public function AddQuestao()
	{
		$id_prova = $this->uri->segment(2);
		$dados = array('id_prova' => $id_prova);
		$this->load->view('admin/addquestao_view', $dados);
	}

	public function Salvar()
	{

		// array $dados recebe os dados do formulário
		$dados = array (
			'enunciado' => $this->input->post('enunciado'),
			'disciplina' => $this->input->post('disciplina'),
			'alt_a' => $this->input->post('alt_a'),
			'alt_b' => $this->input->post('alt_b'),
			'alt_c' => $this->input->post('alt_c'),
			'alt_d' => $this->input->post('alt_d'),
			'alt_correta' => $this->input->post('alt_correta'),
			'id_prova' => $this->input->post('id_prova')
		);

		//Insere os dados no banco e recupera o status dessa operação
		$status = $this->Questao_model->inserir($dados);	

		//Gerando mensagens relacionadas ao status
		if(!$status){
			$this->session->set_flashdata('error', 'Não foi possível inserir a questão.');
		} else {
			$this->session->set_flashdata('success', 'Questão inserida com êxito!');
		}

		$id_prova = $this->uri->segment(2);

		//redireciona para a página de questões para a determinada prova (id_prova)
		redirect(base_url('questoesprova/'.$id_prova));
	}

	public function Editar()
	{
		$id = $this->uri->segment(2);
		$dados['questao'] = $this->Questao_model->getById($id);
		$this->load->view('admin/editar_view', $dados);
	}

	public function Atualizar()
	{
		$dados = array (
			'id' => $this->input->post('id'),
			'enunciado' => $this->input->post('enunciado'),
			'disciplina' => $this->input->post('disciplina'),
			'alt_a' => $this->input->post('alt_a'),
			'alt_b' => $this->input->post('alt_b'),
			'alt_c' => $this->input->post('alt_c'),
			'alt_d' => $this->input->post('alt_d'),
			'alt_correta' => $this->input->post('alt_correta'),
			'id_prova' => $this->input->post('id_prova')
		);
		$status = $this->Questao_model->atualizar($dados['id'], $dados);
		redirect(base_url('editar/'.$dados['id']));
	}

	public function Remover()
	{
		$id = $this->uri->segment(3);
		$id_prova = $this->uri->segment(2);
		$status = $this->Questao_model->deletar($id);
		redirect(base_url('questoesprova/'.$id_prova));
	}

}

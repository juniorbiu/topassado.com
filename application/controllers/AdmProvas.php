<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AdmProvas extends CI_Controller {

	// Método construtor
	function __construct(){
		parent::__construct();
		$this->load->model('Prova_model');
	}

	public function Index()
	{
		$this->load->view('admin/provas_view');
	}
	
	public function salvar()
	{
		$dados = array (
			"descricao" => $this->input->post('descricao'),
			"ano" => $this->input->post('ano')
		);

		//Insere os dados no banco e recupera o status dessa operação
		$status = $this->Prova_model->inserir($dados);	



		//Gerando mensagens relacionadas ao status
		if(!$status){
			echo "Deu ruim";
		} else {
			//Responde com json
			$json = array("nome" => "julio", "sobrenome" => "gomes");
			echo json_encode($json);
		}

	}

	public function getProvas()
	{

		/*
		# Array $dados, índice 'provas' recebe o retorno de getAll()
		*/
		$dados['provas'] = $this->Prova_model->getAll();
		echo json_encode($dados['provas']);

	}

	public function getQuestoesPorProva()
	{
		$id_prova = $this->uri->segment(2);
		$dados['questoes'] = $this->Prova_model->getQuestoesPorProva($id_prova);
		// pego o id da prova para depois usar para adicionar nova questão
		$dados['id_p'] = $id_prova;
		$this->load->view('admin/questoesprova_view', $dados);
		
	}




}
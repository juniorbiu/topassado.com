<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Provas extends CI_Controller {

	// Método construtor
	function __construct(){
		parent::__construct();
		$this->load->model('Prova_model');
	}

	public function Index()
	{
		/*
		# Array $dados, índice 'questoes' recebe o retorno de getAll()
		*/
		$dados['provas'] = $this->Prova_model->getAll();
		$this->load->view('usuario/provas_view', $dados);
	}
	public function resolver()
	{
		$id_prova = $this->uri->segment(2);
		$dados['id_prova'] = $id_prova;
		$this->load->view('usuario/resolverprova_view', $dados);
	}
	public function getQuestoes()
	{
		$id_prova = $this->input->post('id_prova');
		$dados['questoes'] = $this->Prova_model->getQuestoesPorProva($id_prova);
		echo json_encode($dados['questoes']);
	}

	public function getDescricao()
	{
		$id_prova = $this->input->get('id_prova');

		$descricao = $this->Prova_model->getById($id_prova)->descricao;

		echo json_encode($descricao);

	}

}
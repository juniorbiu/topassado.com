<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desempenho extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Desempenho_model');
	}

	public function index()
	{
		$id = $this->session->userdata('usuario')['id'];
		$dados['resultados'] = $this->Desempenho_model->getResultadosById($id);
		$this->load->view('usuario/resultado_view', $dados);
    }
	
	public function salvarResultado()
	{
		$dados = array(
			'id_usuario' => $this->session->userdata('usuario')['id'],
			'descricao' => $this->input->post('descricao'),
			'qtd_questoes' => $this->input->post('qtd_questoes'),       
			'corretas_pt' => $this->input->post('corretas_pt'),
			'corretas_mat' => $this->input->post('corretas_mat'),
			'corretas_total' => $this->input->post('corretas_total'),
			'data' => date("d/m/y")
		);

		$this->Desempenho_model->inserir($dados);

	}

}

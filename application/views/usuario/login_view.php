<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?= base_url('application/assets/images/tp-favicon.ico'); ?>">
	<!-- Links Css externos -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/Login&Cadastro.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>
	<!-- Login -->
	<div id="overLogin" class="overlay">

		<!-- Formulário de Login -->
		<div class="over-content d-flex justify-content-center">
			
			<div class="formUsuario bg-white rounded shadow-lg">
				<header>
					<?php
						// Quando o usuário realizar o cadastro, será redirecionado para essa página e exibirá uma mensagem de sucesso!
						if($this->session->flashdata('cadastro')){
							echo $this->session->flashdata('cadastro');
						}
					?>
					<h1>Login</h1>
				</header>

				<form action="<?=base_url('autenticar');?>" method="post">
					<div class="input-group mb-2">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-at"></i></span>
						</div>
						<input class="form-control" name="email" type="email" placeholder="E-mail">
					</div>
					<div class="input-group mb-2">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>
						</div>
						<input class="form-control" name="senha" type="password" placeholder="Senha">
					</div>
					<input class="btn btn-danger btn-block" type="submit" value="Entrar">
				</form>

				<?php
        			// Se a variável $erro foi inicializada...
					if(isset($mensagens)){
						echo "<p>$mensagens</p>";
					} else if ($this->session->flashdata('erro')) {
						echo $this->session->flashdata('erro');
					}
				?>

				<div class="lc">
					<p>Não tem uma conta?<a href="<?= base_url('cadastro');?>"> Crie uma agora! </a></p>
				</div>
			</div>

		</div>

	</div>

	<!-- Links Js externos -->
	<script type="text/javascript" src="<?= base_url('application/assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/bootstrap.min.js') ?>"></script>
</body>
</html>
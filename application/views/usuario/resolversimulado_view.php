<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Verifica session LOGADO
if($this->session->userdata('usuario')['grupo'] != 1 && $this->session->userdata('usuario')['grupo'] != 2){
	redirect(base_url('login'));
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Simulados</title>
	<link rel="stylesheet" href="<?= base_url('application/assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/principal.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="<?= base_url('application/assets/css/resolver.css') ?>">
</head>
<body>

<div id="mySidenav" class="sidenav">
		<div class="nav-header">
			<img src="<?= base_url('application/assets/images/user.svg');?>" alt="user.png" width="75px" height="75px">
			<span><?php echo $this->session->userdata('usuario')['nome'];?></span>
		</div>
		<div class="nav-list">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</i></a>
  			<a class="nav-item" href="<?= base_url('provas');?>">Provas</a>
			<a class="nav-item" href="<?= base_url('simulado');?>">Simulados</a>
			<a class="nav-item" href="<?= base_url('resultado');?>">Resultados</a>
			<?php
				if($this->session->userdata('usuario')['grupo'] == 2){
					echo "<a class='nav-item' href='".base_url('admprovas')."'>Administrador</a>";
				}
			?>
  			<a class="nav-item sair" href="<?= base_url('');?>"><i class="fas fa-sign-out-alt"></i> Sair </a>
		</div>
  		
	</div>

	<!-- Use any element to open the sidenav -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1">
				<span id="btn-open" onclick="openNav()"><i class="fas fa-bars p-3"></i></span>
			</div>
			<div class="col-md-10">
				<div class="d-flex justify-content-center"> 
					<img src="<?= base_url('application/assets/images/tp-logotipo.png');?>" alt="Logotipo.png" width="150px" height="75px">
				</div>
			</div>
		</div>
	</div>
	
	

	<!-- Page Content -->
	
	<div class="container-fluid" id="main">
	<div class='simulado'>
        
        <header>
			<h1 class="bg-light p-2">Simulado</h1>
			<hr>
        </header>
        <div class='escolha'>
            <p>Escolha o número de questões para fazer seu simulado</p>
            <input type="radio" name="qtdQ" value="2"> 2
            <input type="radio" name="qtdQ" value="4"> 4
            <input type="radio" name="qtdQ" value="6"> 6
            <button id="ok" class="btn btn-info">OK</button>
        </div>  
		


		<div class='prova'>
		<header class='header'>
			<h1 id="descricao"></h1>
			<hr>
		</header>
		<div class="prova-corpo2 mycontainer">
			<div class='tabs'>
				<ul id='tabs'>
				</ul>
			</div>
			<div class="tab-content mycontainer" id='tab-content'>
			</div>
			<div class="btn-responder">
				<button class="btn btn-secondary" onclick="anterior();" id="btn-anterior">Anterior</button>
			</div>
			<div class="btn-responder">
				<button class="btn btn-primary" onclick="proxima();" id="btn-proxima">Proxima</button>
			</div>
			<!-- Button trigger modal -->
			<div class="btn-responder">
				<button type="button" class="btn btn-danger" id="btn-finalizar" data-toggle="modal" data-target="#resultados" onclick="finalizar();">Finalizar</button>
			</div>
		</div>


		
		<div class="myModalCustom">
			<!-- Modal -->
			<div class="modal fade" id="resultados" tabindex="-1" role="dialog" aria-labelledby="resultadosLabel" aria-hidden="true">
  				<div class="modal-dialog" role="document">
    				<div class="modal-content">
      					<div class="modal-header bg-info">
        					<h5 class="modal-title" id="resultadosLabel">Resultados</h5>
        					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          						<span aria-hidden="true">&times;</span>
        					</button>
      					</div>
      					<div class="modal-body">
        					<!-- ... -->
      					</div>
      					<div class="modal-footer">
        					<a class="btn btn-secondary" onclick="fechar()" data-dismiss="modal" >Fechar</a>
        					<a class="btn btn-primary" onclick="salvar()" href="<?= base_url('resultado'); ?>">Salvar</a>
      					</div>
    				</div>
  				</div>
			</div>
		</div>
	</div>
	</div>

	<script type="text/javascript" src="<?= base_url('application/assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/principal.js')?>"></script>
	
	<script>


		// Json prova para armazenar informções sobre
		simulado = {
			'questoes': [],
			'corretasPt': 0,
			'corretasMat': 0,
			'corretasTotal': 0
		};


		$('#ok').click(function(){
			var qtd = $('input[name="qtdQ"]:checked').val();
			$('.prova-corpo2').css({'display': 'block'});
			$('.escolha').css({'display': 'none'});
			// console.log(qtd);
			loadQuestoes(qtd);
		});


		// Pega as questões pelo quantidade
		function loadQuestoes(qtd){
			$.ajax({
				url: "<?= base_url('simulado/questoes')?>",
				// Método de envio
				method: "post",
				// Tipo de retorno
				dataType: "json",
				// Dados que serão enviados
				data: {qtdQuestoes: qtd}, 
				// Função executada se der tudo certo
				success: function(resposta){
					console.log(resposta);
					// junta questoes de portugues com matematica
					if(resposta.portugues != null){
						simulado.questoes = resposta.portugues.concat(resposta.matematica);
					}
					
					// Chama função para Atualizar as Questõe
					listaQuestoes();
					
				}
			});
		}

		function listaQuestoes(){
			$('#tab-content').empty();
			console.log(simulado.questoes.length);
			var item = '';
			if(simulado.questoes == 0){
				item += '<li class="alert alert-info"> Não é possível montar o simulado por enquanto... </li>';
				$('#tab-content').html(item);
			}else {
				for(i=0; i < simulado.questoes.length; i++){
				var numero = i+1;
				item += '<div class="tab-questao" id="#tabs-'+simulado.questoes[i].id+'">';
				item += '<span class="numero"><strong>Questão '+numero+' - '+simulado.questoes[i].disciplina+'</strong></span>'+
						'<span class="enunciado bg-light">'+simulado.questoes[i].enunciado+'</span> <br>'+
						'<div class="alternativas">'+
							'<div class="alternativa">'+
								'<input type="radio" name="altq'+simulado.questoes[i].id+'" value="A"> <span>'+ simulado.questoes[i].alt_a+'</span>'+
							'</div>'+
							'<div class="alternativa">'+
								'<input type="radio" name="altq'+simulado.questoes[i].id+'" value="B"> <span>'+ simulado.questoes[i].alt_b+'</span>'+
							'</div>'+
							'<div class="alternativa">'+
								'<input type="radio" name="altq'+simulado.questoes[i].id+'" value="C"> <span>'+ simulado.questoes[i].alt_c+'</span>'+
							'</div>'+
							'<div class="alternativa">'+
								'<input type="radio" name="altq'+simulado.questoes[i].id+'" value="D"> <span>'+ simulado.questoes[i].alt_d+'</span>'+
							'</div>'+
						'</div>'+
						'</div>';
				$('#tab-content').html(item);
			}
			}
			

			// Mostra a primeira questão
			$('.tab-questao:first-child').addClass('show');

			// Carrega as tabs (abas)
			loadTabs();

		}

		function anterior(){
			var id = parseInt($('.active').attr('id')) -1;
			if(id < 1){
				// $('#btn-anterior').css('visibility', 'hidden');
			}else {
				$('.tab-item').removeClass('active');
				$('.tab-item:nth-child('+id+')').addClass('active');
				$('.tab-questao').removeClass('show');
				$('.tab-questao:nth-child('+id+')').addClass('show');
			}

			if(id < simulado.questoes.length){
				$('#btn-finalizar').css('visibility', 'hidden');
			}
			
		}

		function proxima(){
			var id = parseInt($('.active').attr('id')) + 1;
			if(id > simulado.questoes.length){
				// $('#btn-proxima').prop('readonly', true);
			}else {
				$('.tab-item').removeClass('active');
				$('.tab-item:nth-child('+id+')').addClass('active');
				$('.tab-questao').removeClass('show');
				$('.tab-questao:nth-child('+id+')').addClass('show');
			}

			if(id == simulado.questoes.length){
				$('#btn-finalizar').css('visibility', 'visible');
			}
			
		}


		// Função para carregar e formatar as tabs (abas)
		function loadTabs(){
			var item = '';
			for(i=1; i <= simulado.questoes.length; i++){
				item += '<li class="tab-item" id="'+i+'">';
				item += '<a class="tab-item-link">'+i+'</a>';
				item += '</li>';
				$('#tabs').html(item);
			}

			// Ativa a primeira aba por padrão
			$('.tab-item:first-child').addClass('active');

			// Mudar aba e conteúdo quando clicar
			$('.tab-item').click(function(){
				$('.tab-item').removeClass('active');
				$(this).addClass('active');
				var id = $(this).attr('id');
				$('.tab-questao').removeClass('show');
				$('.tab-questao:nth-child('+id+')').addClass('show');
				if(id == simulado.questoes.length){
					$('#btn-finalizar').css('visibility', 'visible');
				}else {
					$('#btn-finalizar').css('visibility', 'hidden');
				}
			});

		}


		// IMPORTANTE
		// Função para responder prova
		// ######################
		function finalizar(){
			var respostas = [];
			for(var i = 0; i < simulado.questoes.length; i++){
				respostas.push($('input[name="altq'+simulado.questoes[i].id+'"]:checked').val());
			}

			// Fazendo as condicionais
			for(var i = 0; i < simulado.questoes.length; i++){
				if(simulado.questoes[i].disciplina == "Português"){
					if(simulado.questoes[i].alt_correta == respostas[i]){
						simulado.corretasPt += 1;
					}
				}else if (simulado.questoes[i].disciplina == "Matemática"){
					if(simulado.questoes[i].alt_correta == respostas[i]){
						simulado.corretasMat += 1;
					}
				}
			}

			// Cálculo do Total
			simulado.corretasTotal = simulado.corretasPt + simulado.corretasMat;
			
			// Exibir informações no Modal
			$('.modal-body').html(
				'<h1>Português</h1>'+
					'<p>Acertos: '+simulado.corretasPt+'</p>'+

				'<h1>Matemática</h1>'+
					'<p>Acertos: '+simulado.corretasMat+'</p>'+
					
				'<hr>'+
				'<h2>Total: '+simulado.corretasTotal+'/'+simulado.questoes.length+'</h2>'
			);

		}

		// função para salvar os resultados
		function salvar(){
			dados = {
				descricao: "Simulado",
				qtd_questoes: simulado.questoes.length, 
				corretas_pt: simulado.corretasPt,
				corretas_mat: simulado.corretasMat,
				corretas_total: simulado.corretasTotal
			} 
			$.post("<?= base_url('salvarresultado')?>", dados)
    		.done(function(resp){
        		console.log('deu certo');
    		});
		}

		
		function fechar(){
			prova.corretasPt = 0;
			prova.corretasMat = 0;
			prova.corretasTotal = 0;
		}

	</script>
</body>
</html>
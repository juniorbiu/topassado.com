<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Resultados</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/principal.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>
<body>
    
<div id="mySidenav" class="sidenav">
		<div class="nav-header">
			<img src="<?= base_url('application/assets/images/user.svg');?>" alt="user.png" width="75px" height="75px">
			<span><?php echo $this->session->userdata('usuario')['nome'];?></span>
		</div>
		<div class="nav-list">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</i></a>
  			<a class="nav-item" href="<?= base_url('provas');?>">Provas</a>
			<a class="nav-item" href="<?= base_url('simulado');?>">Simulados</a>
			<a class="nav-item" href="<?= base_url('resultado');?>">Resultados</a>
			<?php
				if($this->session->userdata('usuario')['grupo'] == 2){
					echo "<a class='nav-item' href='".base_url('admprovas')."'>Administrador</a>";
				}
			?>
  			<a class="nav-item sair" href="<?= base_url('');?>"><i class="fas fa-sign-out-alt"></i> Sair </a>
		</div>
  		
	</div>

	<!-- Use any element to open the sidenav -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1">
				<span id="btn-open" onclick="openNav()"><i class="fas fa-bars p-3"></i></span>
			</div>
			<div class="col-md-10">
				<div class="d-flex justify-content-center"> 
					<img src="<?= base_url('application/assets/images/tp-logotipo.png');?>" alt="Logotipo.png" width="150px" height="75px">
				</div>
			</div>
		</div>
	</div>
	
	

	<!-- Page Content -->
	
	<div class="container-fluid" id="main">
    <header>
        <h1 class="bg-light p-2">Resultados</h1>
        <hr>
    </header>
    <div class="mycontainer">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th>N° Questões</th>
                    <th><i class="fas fa-check"></i> Português</th>
                    <th><i class="fas fa-check"></i> Matemática</th>
                    <th><i class="fas fa-check"></i> Acertos Totais</th>
                    <th>Data</th>
                </tr>
            </thead>
            <tbody id="resultados">
            <?php 
					if(isset($resultados)){
						foreach ($resultados as $resultado) {
							echo '<tr>';
							echo '<td>'.$resultado->descricao.'</td>';
							echo '<td>'.$resultado->qtd_questoes.'</td>';
							echo '<td>'.$resultado->corretas_pt.'</td>';
                            echo '<td>'.$resultado->corretas_mat.'</td>';
                            echo '<td>'.$resultado->corretas_total.'</td>';
                            echo '<td>'.$resultado->data.'</td>';
							echo '</tr>';
						}
					} else {
                        echo '<tr>';
                        echo '<td colspan="6" class="alert alert-info"> Não há resultados ainda... </td>';
                        echo '</tr>';
                    }
					?>
            </tbody>
        </table>
    </div>
	</div>

    <script type="text/javascript" src="<?= base_url('application/assets/js/principal.js')?>"></script>
</body>
</html>
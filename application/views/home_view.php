<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?= base_url('application/assets/images/tp-favicon.ico'); ?>">
	<!-- Links Css externos -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/home.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>

	<!-- Navbar -->

	<nav class="navbar navbar-expand-lg navbar-light bg-white">
		<div class="container">
			<a class="navbar-brand" href="<?= base_url('home'); ?>">
				<img src="<?= base_url('application/assets/images/tp-logotipo.png'); ?>" alt="logotipo" width="120" height="60">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end" id="navbarContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link btn" href="<?= base_url('login');?>">Login</a>
					</li>
					<li class="nav-item ml-3">
						<a class="nav-link btn btn-outline-danger" href="<?= base_url('cadastro');?>">Cadastre-se</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	
	<div class="container-fluid">
		<div class="row bg-green vitrine">
			<div class="col-md-6 col-12">
				<div class="img-bibli">
					<img class="img-fluid" src="<?= base_url('application/assets/images/estudo.png');?>" alt="materiais">
				</div>
			</div>
			<div class="offset-md-1 col-md-4 letter-white vitrine-texto">
				<h1>Preparação para ingressar no IFRN!</h1>
				<h4>Resolva provas antigas e monte seu simulados</h4>
			</div>
			<div class="vitrine-entre">
				<a href="<?= base_url('cadastro');?>" class="vitrine-comecar">Começar agora</a>
				<a href="<?= base_url('login');?>" class="vitrine-cc">Entrar</a>
			</div>
		</div>
		
		<div class="row bg-white">
			<div class="col-md-12 col-12 text-center">
				<h3 class="letter-green p-3">Provas atualizadas e simulados personalizados</h3>
			</div>
		</div>

		<div class="row bg-white prs">
			<div class="offset-md-2 col-md-4 col-12 text-center">
				<h3 class="border ps">Provas</h3>
				<h5 class="legenda-prova">Provas de versões anteriores do Exame de seleção esperam por você!</h5>

			</div>
			<div class="col-md-4 col-12 text-center">
				<h3 class="border ps">Simulados</h3>
				<h5 class="legenda-simulado">Possibilidade de escolher a quantidade de questões que se adequa ao tempo disponível!</h5>
			</div>
		</div>
		<div class="row bg-red sobre">
			<div class="col-md-12 col-12 text-center">
				<h2 class="letter-white p-3">Sobre</h2>
				<p>O projeto TôPassado surgiu com o intuito de auxiliar os estudos para o ingresso no Instituto Federal disponibilizando online um banco de questões das provas de processos seletivos anteriores.
				</p>
			</div>
			<div class="offset-md-2 col-md-8 col-12 text-center">
				<img class="img-fluid rounded-circle p-2" width="200px" height="150px" src="<?= base_url('application/assets/images/bibli.jpg');?>" alt="instagram">
			</div>
		</div>
		<footer class="row bg-dark">
			<div class="col-md-12 direitos">
				<span>Direitos reservados 2019</span>
			</div>
		</footer>

	</div>

	<!-- Use any element to open/show the overlay navigation menu -->
	<!-- <span onclick="openLogin()">open</span> -->


	<!-- Links Js externos -->
	<script type="text/javascript" src="<?= base_url('application/assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/home.js')?>"></script>
	<script type="text/javascript">
		$('.ps').mouseover(function(){
			var val = $(this).text();
			if(val == "Provas"){
				$('.legenda-prova').fadeIn(500);
			}else if(val == "Simulados"){
				$('.legenda-simulado').fadeIn(500);
			}
		});
		$('.ps').mouseout(function(){
			var val = $(this).text();
			if(val == "Provas"){
				$('.legenda-prova').fadeOut(500);
			}else if(val == "Simulados"){
				$('.legenda-simulado').fadeOut(500);
			}
		});
	</script>
</body>
</html>

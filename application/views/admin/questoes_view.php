<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Verifica session LOGADO
if($this->session->userdata('usuario')['grupo'] != 2){
	redirect(base_url('login'));
}

?>

<?php 
$urlEditar = base_url('editar/');
$urlRemover = base_url('remover/');
$urlAdd = base_url('add/');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Banco de Questões</title>
	<!-- links bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/principal.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/icons.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/jquery.dataTables.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/tp.style.css'); ?>">


</head>
<body>

<div id="mySidenav" class="sidenav">
		<div class="nav-header">
			<img src="<?= base_url('application/assets/images/user.svg');?>" alt="user.png" width="75px" height="75px">
			<span><?php echo $this->session->userdata('usuario')['nome'];?></span>
		</div>
		<div class="nav-list">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</i></a>
  			<a class="nav-item" href="<?= base_url('provas');?>">Provas</a>
			<a class="nav-item" href="<?= base_url('simulado');?>">Simulados</a>
			<a class="nav-item" href="<?= base_url('resultado');?>">Resultados</a>
			<?php
				if($this->session->userdata('usuario')['grupo'] == 2){
					echo "<a class='nav-item' href='".base_url('admprovas')."'>Administrador</a>";
				}
			?>
  			<a class="nav-item sair" href="<?= base_url('');?>"><i class="fas fa-sign-out-alt"></i> Sair </a>
		</div>
  		
	</div>

	<!-- Use any element to open the sidenav -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1">
				<span id="btn-open" onclick="openNav()"><i class="fas fa-bars p-3"></i></span>
			</div>
			<div class="col-md-10">
				<div class="d-flex justify-content-center"> 
					<img src="<?= base_url('application/assets/images/tp-logotipo.png');?>" alt="Logotipo.png" width="150px" height="75px">
				</div>
			</div>
		</div>
	</div>
	
	

	<!-- Page Content -->
	
	<div class="container-fluid" id="main">
	<div id="alerta" class="container">
		<?php if ($this->session->flashdata('error') == TRUE): ?>
			<p class="alert alert-primary"><?php echo $this->session->flashdata('error'); ?></p>
		<?php endif; ?>
		<?php if ($this->session->flashdata('success') == TRUE): ?>
			<p><?php echo $this->session->flashdata('success'); ?></p>
		<?php endif; ?>
	</div>

	<div class="container">
		<header>
			<h1>Banco de Questões</h1>
		</header>
	</div>

	<div class="container">
		<a href="<?= base_url('add') ?>" class="btn btn-info mb-2">Nova</a>
		<a href="<?= base_url('questoes'); ?>"" class="btn btn-secondary mb-2"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span></a>
		<div id="div-tabela" class="table-responsive">
			<table id="tabela-questao" class="table">
				<thead class="table-dark">
					<tr>
						<th>#</th>
						<th>Disciplina</th>
						<th>Ano</th>
						<th>Banca</th>
						<th>Prova</th>
						<th>Opções</th>
					</tr>
				</thead>
				<tbody id="questoes">
					<?php 
					if(isset($questoes)){
						foreach ($questoes as $questao) {
							echo '<tr>';
							echo '<td>'.$questao->id.'</td>';
							echo '<td>'.$questao->disciplina.'</td>';
							echo '<td>'.$questao->ano.'</td>';
							echo '<td>'.$questao->banca.'</td>';
							echo '<td>'.$questao->prova.'</td>';
							echo '<td>
							<a id="btn-editar" href="'.$urlEditar.$questao->id.'" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
							<a id="btn-remover" href="'.$urlRemover.$questao->id.'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
							</td>';
							echo '</tr>';
						}
					}
					?>
				</tbody> 
				<tfoot>
					<tr>
						<th>#</th>
						<th>Disciplina</th>
						<th>Ano</th>
						<th>Banca</th>
						<th>Prova</th>
						<th>Opções</th>
					</tr>
				</tfoot>
			</table>
			
		</div>
	</div>

	</div>


	

	<!-- Link JavaScript -->
	<script type="text/javascript" src="<?= base_url('application/assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/jquery.dataTables.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/bootstrap.min.js') ?>"></script>
	<script>
		$(document).ready(function(){
			$('#tabela-questao').DataTable({
				"scrollY": "400px",
				"scrollCollapse": true
			});
		});
	</script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/principal.js')?>"></script>
	
</body>
</html>
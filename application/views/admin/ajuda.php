<<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Verifica session LOGADO
if($this->session->userdata('usuario')['grupo'] != 2){
	redirect(base_url('login'));
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Editar</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css'); ?>">
</head>
<body>
	<h1>Editar Questão</h1>
	<div>
		<a href="<?= base_url('questoes'); ?>"><button type="button">Ver Questões</button></a>
	</div>
	<div>
		<form action="<?= base_url('atualizar'); ?>" method="post">
			
			<input name="id" value="<?= $questao->id; ?>">
			<select name="disciplina">
				<?php 
					if ($questao->disciplina == "Português") {
						echo("<option value='Português' selected = 'selected'>Português</option>
						<option value='Matemática'>Matemática</option>");
					}else{
						echo("<option value='Português'>Português</option>
  						<option value='Matemática' selected = 'selected'>Matemática</option>");
					}
				?>
			</select>	
			<input name="ano" type="number" min="2000" max="2018" placeholder="Ano" value="<?= $questao->ano; ?>">
			<input name="banca" type="text" placeholder="Banca" value="<?= $questao->banca; ?>">
			<input name="prova" type="text" placeholder="Prova" value="<?= $questao->prova; ?>">
			<input name="enunciado" type="text" id="enunciado" cols="10" rows="5" placeholder="Digite o enunciado aqui..." value="<?= $questao->enunciado; ?>">
			<input name="img" type="file" value="<?= $questao->img; ?>">
			<input name="alt_A" type="text" placeholder="alt A" value="<?= $questao->alt_A; ?>">
			<input name="alt_B" type="text" placeholder="alt B" value="<?= $questao->alt_B; ?>">
			<input name="alt_C" type="text" placeholder="alt C" value="<?= $questao->alt_C; ?>">
			<input name="alt_D" type="text" placeholder="alt D" value="<?= $questao->alt_D; ?>">
			<select name="alt_correta" value="<?= $questao->alt_correta; ?>">
				<option value="A">A</option>
  				<option value="B">B</option>
  				<option value="C">C</option>
  				<option value="D">D</option>
  				<?php 
					if ($questao->alt_correta == "A") {
						echo("<option value='A' selected = 'selected'>A</option>
  							  <option value='B'>B</option>
  							  <option value='C'>C</option>
  							  <option value='D'>D</option>");
					}elseif ($questao->alt_correta == "B"){
						echo("<option value='A'>A</option>
  							  <option value='B' selected = 'selected'>B</option>
  							  <option value='C'>C</option>
  							  <option value='D'>D</option>");
					}elseif ($questao->alt_correta == "C"){
						echo("<option value='A' selected = 'selected'>A</option>
  							  <option value='B'>B</option>
  							  <option value='C' selected = 'selected'>C</option>
  							  <option value='D'>D</option>");
					}else{
						echo("<option value='A' selected = 'selected'>A</option>
  							  <option value='B'>B</option>
  							  <option value='C'>C</option>
  							  <option value='D' selected = 'selected'>D</option>");
					}
				?>
			</select>
			
			<button type="submit">Atualizar</button>

	
		</form>
	</div>
</body>
</html>
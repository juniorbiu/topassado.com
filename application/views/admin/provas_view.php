<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Verifica session LOGADO
if($this->session->userdata('usuario')['grupo'] != 2){
	redirect(base_url('login'));
}

?>

<?php 
$urlQuestoes = base_url('questoesprova/');
$urlRemover = base_url('remover/');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Manter Provas</title>
	<!-- links bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/icons.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/tp.style.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/principal.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<style>
		h4{font-size: 1rem; padding-top: 6px;}
	</style>

</head>
<body>

<div id="mySidenav" class="sidenav">
		<div class="nav-header">
			<img src="<?= base_url('application/assets/images/user.svg');?>" alt="user.png" width="75px" height="75px">
			<span><?php echo $this->session->userdata('usuario')['nome'];?></span>
		</div>
		<div class="nav-list">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</i></a>
  			<a class="nav-item" href="<?= base_url('provas');?>">Provas</a>
			<a class="nav-item" href="<?= base_url('simulado');?>">Simulados</a>
			<a class="nav-item" href="<?= base_url('resultado');?>">Resultados</a>
			<?php
				if($this->session->userdata('usuario')['grupo'] == 2){
					echo "<a class='nav-item' href='".base_url('admprovas')."'>Administrador</a>";
				}
			?>
  			<a class="nav-item sair" href="<?= base_url('');?>"><i class="fas fa-sign-out-alt"></i> Sair </a>
		</div>
  		
	</div>

	<!-- Use any element to open the sidenav -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1">
				<span id="btn-open" onclick="openNav()"><i class="fas fa-bars p-3"></i></span>
			</div>
			<div class="col-md-10">
				<div class="d-flex justify-content-center"> 
					<img src="<?= base_url('application/assets/images/tp-logotipo.png');?>" alt="Logotipo.png" width="150px" height="75px">
				</div>
			</div>
		</div>
	</div>
	
	

	<!-- Page Content -->
	
	<div class="container-fluid" id="main">
	<div id="alerta" class="">
		<?php if ($this->session->flashdata('error') == TRUE): ?>
			<p class="alert alert-primary"><?php echo $this->session->flashdata('error'); ?></p>
		<?php endif; ?>
		<?php if ($this->session->flashdata('success') == TRUE): ?>
			<p><?php echo $this->session->flashdata('success'); ?></p>
		<?php endif; ?>
	</div>

	<div class="">
		<header>
			<h1>Provas</h1>
			<hr>
		</header>
	</div>

	<div class="myform">	
		<form>
			<div class="form-row">
				<div class="col-2">
					<h4>Nova Prova</h4>
				</div>
				<div class="col-4">
					<input type="text" class="form-control" name="descricao" placeholder="Descrição">
				</div>
				<div class="col-2">
					<input type="number" min="2010" max="2020" class="form-control" name="ano" placeholder="Ano">
				</div>
				<div class="col-2">
					<button type="submit" class="btn btn-info">Salvar</button>
				</div>
			</div>
		</form>
		
	</div>

	<div class="mycontainer">
		<ul id="provas">
			<!-- Lista dinâmica -->
		</ul>
	</div>
	</div>


	


	<!-- Link JavaScript -->
	<script type="text/javascript" src="<?= base_url('application/assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/bootstrap.min.js') ?>"></script>
	<script>

		//Tudo está carregado...
		$(document).ready(function(){

			//Chama função para atualizar lista de provas
			atualizarListaProvas();

			//função para salvar dados
			function salvar(descricao, ano){
				$.ajax({
					url: "<?= base_url('salvarprova')?>",
					//Método de envio
					method: "post",
					//Tipo de retorno
					dataType: "json",
					//Dados que serão enviados
					data: {descricao: descricao, ano: ano}, 
					//Função executada se der tudo certo
					success: function(resposta){
						// Chama função para Atualizar as provas
						atualizarListaProvas();
					}
				});
			}

			function atualizarListaProvas(){
				$.ajax({
					url: "<?= base_url('carregarprovas')?>",
					dataType: "json",
					success: function(resposta){
						var item = '';
						if(resposta == null || resposta == undefined){
							item += '<li class="alert alert-info">Não há provas cadastradas ainda...</li>';
						}else {
							for(i=0; i < resposta.length; i++){
							item += '<div class="prova-caixa">'+
										'<div class="prova-info">'+
											'<i class="far fa-file-alt prova-icon"></i>'+
											'<span>'+ resposta[i].descricao+'</span>'+
										'</div>'+
										'<div class="prova-acesso">'+
											'<a href="<?=$urlQuestoes?>'+resposta[i].id+'">Acessar</a>'+
										'</div>'+
									'</div>';
							}		
						}
						
						$('#provas').html(item);
					}
				});
			}

			//Evento que envia formulário
			$('form').submit(function(e){
				// Nõo recarrega a página
				e.preventDefault();

				//Pega os valores do formulário no momento de submeter
				var descricao = $("input[name='descricao']").val();
				var ano = $("input[name='ano']").val();

				// chama função para salvar dados
				salvar(descricao, ano);
			});

		});
	</script>
	<script type="text/javascript" src="<?= base_url('application/assets/js/principal.js')?>"></script>
</body>
</html>
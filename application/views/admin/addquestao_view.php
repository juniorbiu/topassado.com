<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Verifica session LOGADO
if($this->session->userdata('usuario')['grupo'] != 2){
	redirect(base_url('login'));
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Questões</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('application/assets/css/principal.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>


<div id="mySidenav" class="sidenav">
		<div class="nav-header">
			<img src="<?= base_url('application/assets/images/user.svg');?>" alt="user.png" width="75px" height="75px">
			<span><?php echo $this->session->userdata('usuario')['nome'];?></span>
		</div>
		<div class="nav-list">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</i></a>
  			<a class="nav-item" href="<?= base_url('provas');?>">Provas</a>
			<a class="nav-item" href="<?= base_url('simulado');?>">Simulados</a>
			<a class="nav-item" href="<?= base_url('resultado');?>">Resultados</a>
			<?php
				if($this->session->userdata('usuario')['grupo'] == 2){
					echo "<a class='nav-item' href='".base_url('admprovas')."'>Administrador</a>";
				}
			?>
  			<a class="nav-item sair" href="<?= base_url('');?>"><i class="fas fa-sign-out-alt"></i> Sair </a>
		</div>
  		
	</div>

	<!-- Use any element to open the sidenav -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1">
				<span id="btn-open" onclick="openNav()"><i class="fas fa-bars p-3"></i></span>
			</div>
			<div class="col-md-10">
				<div class="d-flex justify-content-center"> 
					<img src="<?= base_url('application/assets/images/tp-logotipo.png');?>" alt="Logotipo.png" width="150px" height="75px">
				</div>
			</div>
		</div>
	</div>
	
	

	<!-- Page Content -->
	
	<div class="container-fluid" id="main">
	<div class="mt-4">

<h3>Nova Questão</h3>
<hr>

<div class="mycontainer">
<form action="<?= base_url('salvar/'.$id_prova); ?>" method="post">
	<div class="form-group">
		<textarea name="enunciado" placeholder="Enunciado" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
	</div>
	<div class="row mt-2 mb-2">
		<div class="col-3">
			<label for="">Prova</label>
			<input name="id_prova" class="form-control" readonly type="text" placeholder="..." value="<?= $id_prova ?>">
		</div>
		<div class="col-3">
			<label for="">Alternativa Correta</label>
			<select name="alt_correta" class="form-control">
				<option value="A">A</option>
				<option value="B">B</option>
				<option value="C">C</option>
				<option value="D">D</option>
			</select>
		</div>
		<div class="col-6">
			<label for="">Disciplina</label>
			<select name="disciplina" class="form-control">
				<option value="Português">Português</option>
				<option value="Matemática">Matemática</option>
			</select>
		</div>
	</div>
	<div class="row mt-4">
		<h5 class="col-12">Alternativas</h5>
	</div>
	<div class="row">
		<div class="col">
			<input name="alt_a" type="text" class="form-control" placeholder="alternativa A">
		</div>
		<div class="col">
			<input name="alt_b" type="text" class="form-control" placeholder="alternativa B">
		</div>
	</div>
	<div class="row mt-2">
		<div class="col">
			<input name="alt_c" type="text" class="form-control" placeholder="alternativa C">
		</div>
		<div class="col">
			<input name="alt_d" type="text" class="form-control" placeholder="alternativa D">
		</div>
	</div>
	
	<div class="form-group">
		
	</div>
	
	<button class="btn btn-success" type="submit">Salvar</button>
</form>
</div>
	</div>
</div>



	<script type="text/javascript" src="<?= base_url('application/assets/js/principal.js')?>"></script>
	
</body>
</html>
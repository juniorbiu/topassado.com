<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prova_model extends CRUD_model {

	function __construct()
	{
		parent::__construct();
		$this->tabela = 'tp_provas';
	}


	public function getQuestoesPorProva($id_prova)
  	{
  		if(is_null($id_prova))
  			return false;

  		$this->db->where('id_prova', $id_prova);
  		$query = $this->db->get('tp_questoes');

  		if($query->num_rows() > 0){
  			return $query->result();
  		} else {
  			return null;
  		}
  	}

}
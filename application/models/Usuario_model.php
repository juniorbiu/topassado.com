<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CRUD_model {

	function __construct()
	{
		parent::__construct();
		$this->tabela = 'tp_usuarios';
    }
    // Método para autenticar usuário
    public function autenticar($email, $senha)
    {   
        // Cláusulas (condições) where
        $this->db->where('email', $email);
        $this->db->where('senha', $senha);

        // Retorna a consulta pegando todos dentro das condições 'where'
        $query = $this->db->get($this->tabela);

        // Retorna true se for encontrado algum usuário
        if($query->num_rows() > 0){
            return $query->row();
        }
    }

}
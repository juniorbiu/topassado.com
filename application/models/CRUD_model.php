<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CRUD_model extends CI_Model {

	// Variável que define o nome da tabela
	var $tabela = "";
	
	// Método Construtor
	function __construct()
	{
		parent::__construct();
	}

	/**
  	* Insere um registro na tabela
  	*
  	* @param array $dados -> Dados a serem inseridos
  	*
  	* @return boolean
  	*/

  	public function inserir($dados)
	{	
		if(!isset($dados))
			return false;

		return $this->db->insert($this->tabela, $dados);
	}

	/**
  	* Recupera um registro a partir de um ID
  	*
  	* @param integer $id ID do registro a ser recuperado
  	*
  	* @return array
  	*/

  	public function getById($id)
  	{
  		if(is_null($id))
  			return false;

  		$this->db->where('id', $id);
  		$query = $this->db->get($this->tabela);

  		if($query->num_rows() > 0){
  			return $query->row();
  		} else {
  			return null;
  		}
  	}

  	/**
  	* Lista todos os registros da tabela
  	*
  	* @param string $sort Campo para ordenação dos registros
  	*
  	* @param string $order Tipo de ordenação: ASC ou DESC
  	*
  	* @return array
  	*/

	public function getAll($sort = 'id', $order = 'asc')
	{	
		$this->db->order_by($sort, $order);
		$query = $this->db->get($this->tabela);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	/**
  	* Atualiza um registro na tabela
  	*
  	* @param integer $int ID do registro a ser atualizado
  	*
  	* @param array $data Dados a serem inseridos
  	*
  	* @return boolean
  	*/
		
	public function atualizar($id, $dados)
	{
		if(is_null($id) || !isset($dados))
			return false;

		$this->db->where('id', $id);
		return $this->db->update($this->tabela, $dados);
	}


	/**
  	* Remove um registro na tabela
  	*
  	* @param integer $int ID do registro a ser removido
  	*
  	*
  	* @return boolean
  	*/
	public function deletar($id)
	{
		if(is_null($id))
			return false;

		$this->db->where('id', $id);
		return $this->db->delete($this->tabela);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questao_model extends CRUD_model {

	function __construct()
	{
		parent::__construct();
		$this->tabela = 'tp_questoes';
	}

	public function ptQuestoes($qtd)
	{
		$n = $qtd/2;

		$this->db->order_by('id', 'RANDOM');
		$this->db->where('disciplina', 'Português');
		$this->db->limit($n);
		$query = $this->db->get($this->tabela);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	public function matQuestoes($qtd)
	{
		$n = $qtd/2;

		$this->db->order_by('id', 'RANDOM');
		$this->db->where('disciplina', 'Matemática');
		$this->db->limit($n);
		$query = $this->db->get($this->tabela);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}
	

}
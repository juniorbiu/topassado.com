<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desempenho_model extends CRUD_model {

	function __construct()
	{
		parent::__construct();
		$this->tabela = 'tp_resultados';
    }
    
    public function getResultadosById($id_usuario)
  	{
  		if(is_null($id_usuario))
  			return false;

  		$this->db->where('id_usuario', $id_usuario);
  		$query = $this->db->get($this->tabela);

  		if($query->num_rows() > 0){
  			return $query->result();
  		} else {
  			return null;
  		}
  	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['admin'] = 'PaginaAdmin';
$route['login'] = 'Usuario/Login';
$route['autenticar'] = 'Usuario/autenticar';
$route['cadastrar'] = 'Usuario/cadastrar';
$route['cadastro'] = 'Usuario/Cadastro';
$route['questoes'] = 'Questao';
$route['questoesprova/(:num)'] = 'AdmProvas/getQuestoesPorProva/$1';
$route['provas'] = 'Provas';
$route['getDescricaoProva'] = 'Provas/getDescricao';
$route['salvarresultado'] = 'Desempenho/salvarResultado';
$route['admprovas'] = 'AdmProvas';
$route['add/(:num)'] = 'Questao/addQuestao/$1';
$route['salvar/(:num)'] = 'Questao/Salvar/$1';
$route['resolver/(:num)'] = 'Provas/resolver/$1';
$route['getquestoes'] = 'Provas/getQuestoes';
$route['simulado'] = 'Simulado';
$route['resultado'] = 'Desempenho';
$route['principal'] = 'Principal';
$route['Principal'] = 'Usuario';
$route['simulado/questoes'] = 'Simulado/randomQuestoes';
$route['salvarprova'] = 'AdmProvas/salvar';
$route['carregarprovas'] = 'AdmProvas/getProvas';
$route['editar/(:num)'] = 'Questao/Editar/$1';
$route['atualizar'] = 'Questao/Atualizar';
$route['remover/(:num)/(:num)'] = 'Questao/Remover';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
